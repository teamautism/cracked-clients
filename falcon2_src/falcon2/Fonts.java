package falcon2;

import java.awt.Font;
import java.io.File;
import java.io.InputStream;
import falcon2.gui.*;

public class Fonts {
	public static CustomFont font1 = new CustomFont(getStream("/assets/HelveticaNeue.otf"), 20F);
	public static CustomFont font1b = new CustomFont(getStream("/assets/HelveticaNeue.otf"), 15F);
	public static CustomFont font2 = new CustomFont(getStream("/assets/HelveticaNeue.otf"), 17F);
	
	private static InputStream getStream(String font) {
		return Fonts.class.getResourceAsStream(font);
	}
}

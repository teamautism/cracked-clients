package falcon2.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class FileUtils {
	public static void writeFile(File file, ArrayList<String> contents) {
		try {
			if (!file.exists())
				file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for (String line : contents)
				bw.write(line);
			bw.close();
		} catch (Exception e) {}
	}

	public static ArrayList<String> readFile(File file) {
		String path = file.getPath();
		ArrayList<String> contents = new ArrayList<String>();
		BufferedReader br = null;
		try {
			String currentLine;
			br = new BufferedReader(new FileReader(path));
			while ((currentLine = br.readLine()) != null)
				contents.add(currentLine);
		} catch (Exception e) {
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (Exception ex) {}
		}
		return contents;
	}
}

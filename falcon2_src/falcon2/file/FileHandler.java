package falcon2.file;

import java.io.File;

public class FileHandler {
	private File file;
	
	public FileHandler(File file) {
		this.file = file;
	}
	
	public File getFile() {
		return file;
	}
	
	public void saveFile() {
		
	}
	
	public void loadFile() {
		
	}
	
	public boolean doesFileExist() {
		return file.exists();
	}
}
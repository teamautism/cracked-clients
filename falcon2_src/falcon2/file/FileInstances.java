package falcon2.file;

import java.util.ArrayList;
import falcon2.Falcon2;
import falcon2.file.files.*;

public class FileInstances {
	private static ArrayList<FileHandler> fileHandlers;
	public FileHandlerFriends fileHandlerFriends = new FileHandlerFriends();
	public FileHandlerSearch fileHandlerSearch = new FileHandlerSearch();
	public FileHandlerSettings fileHandlerSettings = new FileHandlerSettings();
	
	public FileInstances() {
		fileHandlers = new ArrayList<FileHandler>();
		fileHandlers.add(fileHandlerFriends);
		fileHandlers.add(fileHandlerSearch);
		fileHandlers.add(fileHandlerSettings);
	}
	
	public static ArrayList<FileHandler> getFileHandlers() {
		return fileHandlers;
	}
}
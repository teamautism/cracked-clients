package falcon2.gui;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;
import org.lwjgl.input.Keyboard;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.src.*;
import net.minecraft.util.Session;

public class GuiAccountLogin extends GuiScreen {
	public String status = "\247eWaiting...";
	public static GuiScreen lastScreen;
	public TextField usernameField, passwordField;

	public GuiAccountLogin(GuiScreen lastScreen) {
		this.lastScreen = lastScreen;
	}

	@Override
	public void drawScreen(int i, int j, float k) {
		Minecraft mc = Minecraft.getMinecraft();
		int width = mc.displayWidth / 2;
		int height = mc.displayHeight / 2;
		drawDefaultBackground();
		super.drawScreen(i, j, k);
		usernameField.drawTextBox();
		passwordField.drawTextBox();
		String title = "Account Login";
		int title_width = mc.fontRenderer.getStringWidth(title) / 2;
		mc.fontRenderer.drawStringWithShadow(title, width / 2 - title_width, height / 2 - 85, 0xffffffff);
		int s_width = mc.fontRenderer.getStringWidth(status) / 2;
		mc.fontRenderer.drawStringWithShadow(status, width / 2 - s_width, height / 2 - 73, 0xffffffff);
	}

	@Override
	public void initGui() {
		Minecraft mc = Minecraft.getMinecraft();
		int width = mc.displayWidth / 2;
		int height = mc.displayHeight / 2;
		int y = height / 2;
		usernameField = new TextField(mc.fontRenderer, width / 2 - 100, y - 26 - 24, 200, 20);
		usernameField.setMaxStringLength(200);
		passwordField = new TextField(mc.fontRenderer, width / 2 - 100, y - 26, 200, 20, true);
		passwordField.setMaxStringLength(200);
		buttonList.add(new GuiButton(1, width / 2 - 100, y + 2, "Login"));
		buttonList.add(new GuiButton(2, width / 2 - 100, y + 26, "Go Back"));
	}

	@Override
	protected void actionPerformed(GuiButton g) {
		Minecraft mc = Minecraft.getMinecraft();
		switch (g.id) {
			case 1:
				if (passwordField.getText().length() > 0)
					login(usernameField.getText(), passwordField.getText());
				else if(usernameField.getText().contains(":")) {
					try{
						login(usernameField.getText().split(":")[0], usernameField.getText().split(":")[1]);
					} catch (Exception e) {}
				} else {
					mc.session.username = usernameField.getText().trim();
					status = "\247aUsername changed!";
				}
			break;
			case 2:
				mc.displayGuiScreen(lastScreen);
			break;
		}
	}

	@Override
	public void updateScreen() {
		usernameField.updateCursorCounter();
		passwordField.updateCursorCounter();
	}

	@Override
	public void mouseClicked(int i, int j, int k) {
		usernameField.mouseClicked(i, j, k);
		passwordField.mouseClicked(i, j, k);
		super.mouseClicked(i, j, k);
	}

	@Override
	protected void keyTyped(char c, int k) {
		Minecraft mc = Minecraft.getMinecraft();
		if (k == Keyboard.KEY_ESCAPE)
			mc.displayGuiScreen(lastScreen);
		if (k == Keyboard.KEY_TAB) {
			usernameField.setFocused(!usernameField.isFocused());
			passwordField.setFocused(!passwordField.isFocused());
		}
		usernameField.textboxKeyTyped(c, k);
		passwordField.textboxKeyTyped(c, k);
	}

	public void login(final String username, final String password) {
		final Minecraft mc = Minecraft.getMinecraft();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					status = "\247eLogging in...";
					String encodedUrl = "user=";
					encodedUrl += URLEncoder.encode(username, "UTF-8");
					encodedUrl += "&password=";
					encodedUrl += URLEncoder.encode(password, "UTF-8");
					encodedUrl += "&version=";
					encodedUrl += 13;
					String loginString = executePost("https://login.minecraft.net/", encodedUrl);
					if (loginString == null || !loginString.contains(":")) {
						status = "\247cLogin failed!";
						usernameField.setText("");
						passwordField.setText("");
						return;
					}
					String splitter[] = loginString.split(":");
					mc.session = new Session(splitter[2].trim(), splitter[4].trim(), splitter[3].trim());
					status = "\247aLogged in!";
				} catch (UnsupportedEncodingException e) {
				} catch (Exception e2) {}
			}
		}).start();
	}

	public String executePost(String a, String b) {
		HttpsURLConnection urlc = null;
		try {
			try {
				URL url = new URL(a);
				urlc = (HttpsURLConnection) url.openConnection();
				urlc.setRequestMethod("POST");
				urlc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				urlc.setRequestProperty("Content-Length", Integer.toString(b.getBytes().length));
				urlc.setRequestProperty("Content-Language", "en-US");
				urlc.setUseCaches(false);
				urlc.setDoInput(true);
				urlc.setDoOutput(true);
				urlc.connect();
				DataOutputStream dStream = new DataOutputStream(urlc.getOutputStream());
				dStream.writeBytes(b);
				dStream.flush();
				dStream.close();
				InputStream iStream = urlc.getInputStream();
				BufferedReader bReader = new BufferedReader(new InputStreamReader(iStream));
				StringBuffer sBuffer = new StringBuffer();
				String c1;
				while ((c1 = bReader.readLine()) != null) {
					sBuffer.append(c1);
					sBuffer.append('\r');
				}
				bReader.close();
				String c2 = sBuffer.toString();
				return c2;
			} catch (Exception e) {}
			return null;
		} finally {
			if (urlc != null)
				urlc.disconnect();
		}
	}
}
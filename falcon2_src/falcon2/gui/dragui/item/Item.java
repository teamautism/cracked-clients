package falcon2.gui.dragui.item;

public class Item {
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void drawScreen(int i, int j, float k) {
		
	}
	
	public void mouseClicked(int i, int j, int k) {
		
	}
	
	public void mouseMovedOrUp(int i, int j, int k) {
		
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
}
package falcon2.gui.dragui.item;

import static falcon2.gui.Gui.*;
import falcon2.Falcon2;
import falcon2.Wrapper;
import falcon2.gui.CustomFont;
import falcon2.modules.Category;
import falcon2.modules.Module;

public class ItemButton extends Item {
	private CustomFont font;
	private Module module;
	private Category category;
	private int hoverColor = 0x00555555;
	private long last = -1L, delay = 50L;
	
	public ItemButton(Module module, Category category) {
		setModule(module);
		setCategory(category);
		font = Falcon2.getFonts().font2;
	}
	
	@Override
	public void drawScreen(int i, int j, float k) {
		drawRectWithBorder(getX(), getY(), getX() + getWidth(), getY() + getHeight(), 0x30333333, 0xaf000000, 0.5F);
		drawGradientRect(getX() + 0.5F, getY() + 0.5F, getX() + getWidth() - 0.5F, getY() + getHeight() - 0.5F, 0x00, 0x40000000);
		if (last == -1L || Wrapper.getSysTime() >= last + delay) {
			last = Wrapper.getSysTime();
			if (isHovering(i, j)) {
				if (getHoverColor() == 0x20555555)
					setHoverColor(0x30555555);
				if (getHoverColor() == 0x10555555)
					setHoverColor(0x20555555);
				if (getHoverColor() == 0x00555555)
					setHoverColor(0x10555555);
			} else {
				if (getHoverColor() == 0x10555555)
					setHoverColor(0x00555555);
				if (getHoverColor() == 0x20555555)
					setHoverColor(0x10555555);
				if (getHoverColor() == 0x30555555)
					setHoverColor(0x20555555);
			}
			if (getEnabled())
				setHoverColor(0x30555555);
		}
		if (getHoverColor() >= 0x00555555 && getHoverColor() <= 0x30555555) {
			drawRect(getX() + 0.5F, getY() + 0.5F, getX() + getWidth() - 0.5F, getY() + getHeight() - 0.5F, getHoverColor());
		}
		drawRectWithBorder(getX() + 0.5F, getY() + 0.5F, getX() + getWidth() - 0.5F, getY() + getHeight() - 0.5F, 0x00, 0x30555555, 0.5F);
		int textWidth = font.getStringWidth(getText());
		font.drawStringWithShadow(getText(), getX() + (getWidth() / 2) - (textWidth / 2), getY() + 4, getEnabled() ? 0xffff5555 : 0xff666666);
	}
	
	@Override
	public void mouseClicked(int i, int j, int k) {
		if (k == 0 && isHovering(i, j))
			getModule().toggle();
	}
	
	@Override
	public int getHeight() {
		return 14;
	}
	
	public Module getModule() {
		return module;
	}
	
	public String getText() {
		return getModule().getName();
	}
	
	public Category getCategory() {
		return category;
	}
	
	public int getHoverColor() {
		return hoverColor;
	}
	
	public boolean getEnabled() {
		return getModule().getEnabled();
	}
	
	public boolean isHovering(int i, int j) {
		return i >= getX() && i <= getX() + getWidth() && j >= getY() && j <= getY() + getHeight();
	}
	
	public void setModule(Module module) {
		this.module = module;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public void setHoverColor(int hoverColor) {
		this.hoverColor = hoverColor;
	}
}
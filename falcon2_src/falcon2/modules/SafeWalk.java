package falcon2.modules;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;

import org.lwjgl.input.Keyboard;

import falcon2.modules.events.*;

public class SafeWalk extends Module {
	public SafeWalk() {
		super("SafeWalk", Keyboard.KEY_COMMA, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventSafeWalk) {
			EventSafeWalk event2 = (EventSafeWalk) event;
			Object[] objects = event2.getObjects();
			Entity entity = (Entity) objects[0];
			if (entity instanceof EntityPlayer && getPlayer().onGround) {
				handleSafeWalk((Double) objects[1], (Double) objects[2], (Double) objects[3], (Double) objects[4], event2);
			}
		}
	}
	
	private void handleSafeWalk(double par1, double par5, double var13, double var17, EventSafeWalk event) {
		double var21;
		for(var21 = 0.05D; par1 != 0.0D && getWorld().getCollidingBoundingBoxes(getPlayer(), getPlayer().boundingBox.getOffsetBoundingBox(par1, -1.0D, 0.0D)).isEmpty(); var13 = par1) {
			if(par1 < var21 && par1 >= -var21)
				par1 = 0.0D;
			else if(par1 > 0.0D)
				par1 -= var21;
			else
				par1 += var21;
		}
		for(; par5 != 0.0D && getWorld().getCollidingBoundingBoxes(getPlayer(), getPlayer().boundingBox.getOffsetBoundingBox(0.0D, -1.0D, par5)).isEmpty(); var17 = par5) {
			if(par5 < var21 && par5 >= -var21)
				par5 = 0.0D;
			else if(par5 > 0.0D)
				par5 -= var21;
			else
				par5 += var21;
		}
		while(par1 != 0.0D && par5 != 0.0D && getWorld().getCollidingBoundingBoxes(getPlayer(), getPlayer().boundingBox.getOffsetBoundingBox(par1, -1.0D, par5)).isEmpty()) {
			if(par1 < var21 && par1 >= -var21)
				par1 = 0.0D;
			else if(par1 > 0.0D)
				par1 -= var21;
			else
				par1 += var21;
			if(par5 < var21 && par5 >= -var21)
				par5 = 0.0D;
			else if(par5 > 0.0D)
				par5 -= var21;
			else
				par5 += var21;
			var13 = par1;
			var17 = par5;
		}
		event.setObjectsEdit(new Object[]{par1, par5, var13, var17});
	}
}

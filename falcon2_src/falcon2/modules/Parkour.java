package falcon2.modules;

import net.minecraft.network.play.client.C03PacketPlayer;

import org.lwjgl.input.Keyboard;

import falcon2.modules.commands.Command;
import falcon2.modules.commands.CommandManager;
import falcon2.modules.events.*;

public class Parkour extends Module {
	public static float distance = 3F;
	
	public Parkour() {
		super("Parkour", Keyboard.KEY_O, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) {
				if (getPlayer().fallDistance >= distance) {
					getPlayer().fallDistance = 0;
					getSendQueue().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(getPlayer().posX, getPlayer().boundingBox.minY + 2D, getPlayer().posY + 2D, getPlayer().posZ, getPlayer().onGround));
				}
			}
		}
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("pkd") {
			@Override
			public boolean runCommand(String arguments) {
				distance = Float.parseFloat(arguments);
				addChatMessage("Parkour fall distance set to '\247e" + distance + "\247f'.");
				return true;
			}
		});
	}
}

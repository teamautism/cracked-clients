package falcon2.modules;

import org.lwjgl.input.Keyboard;

import falcon2.Falcon2;
import falcon2.modules.events.*;

public class Step extends Module {
	public Step() {
		super("Step", Keyboard.KEY_N, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (getModuleByName("Phase").getEnabled())
			return;
		if (getPlayer().isCollidedHorizontally && getGameSettings().keyBindJump.pressed) {
			getPlayer().onGround = false;
			return;
		}
		if (event instanceof EventMotionUpdate) {
			EventMotionUpdate event2 = (EventMotionUpdate) event;
			if (event2.getState() == State.POST) {
				if (getPlayer().isCollidedHorizontally && getPlayer().onGround) {
					if (getPlayer().isInWater()) {
						getPlayer().motionY = 0.4D;
					} else {
						getPlayer().boundingBox.offset(0, 1.07D, 0);
						if (getPlayer().fallDistance <= 0.0F)
							getPlayer().motionY = -3D;
					}
		        }
			}
		}
	}
}

package falcon2.modules;

import falcon2.modules.events.*;

public class AntiVelocity extends Module {
	public AntiVelocity() {
		super("AntiVelocity", null, Category.COMBAT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (event instanceof EventVelocity) {
			EventVelocity event2 = (EventVelocity) event;
			if (event2.getEntity() != null) {
				if (event2.getEntity() == getPlayer() && getEnabled())
					event2.setCanceled(true);
			} else {
				event2.setCanceled(true);
			}
		}
	}
}

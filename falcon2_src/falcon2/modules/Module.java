package falcon2.modules;

import org.lwjgl.opengl.GL11;

import falcon2.*;
import falcon2.modules.commands.CommandManager;
import falcon2.modules.events.*;

public class Module extends Wrapper {
	private String name;
	private Integer keybind;
	private Category category;
	private int arrayColor;
	private boolean enabled;
	private boolean onArray;
	private boolean hidden;
	protected RenderUtils renderUtils;
	
	public Module(String name, Integer keybind, Category category, boolean enabled) {
		this.name = name;
		this.keybind = keybind;
		this.category = category;
		this.arrayColor = Category.getColor(this.category);
		this.enabled = enabled;
		this.onArray = true;
		this.renderUtils = new RenderUtils();
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getKeybind() {
		return keybind;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public int getArrayColor() {
		return category.getColor(category);
	}
	
	public boolean getEnabled() {
		return enabled;
	}
	
	public boolean getHidden() {
		return hidden;
	}
	
	public boolean getOnArray() {
		return onArray;
	}
	
	public void setKeybind(Integer keybind) {
		this.keybind = keybind;
	}
	
	public void onEvent(Event event) {
		
	}
	
	public void loadCommands(CommandManager commandManager) {
		
	}
	
	public void toggle() {
		if (getHidden())
			return;
		if (getEnabled())
			onDisabled();
		else
			onEnabled();
	}
	
	public void onEnabled() {
		enabled = true;
	}
	
	public void onDisabled() {
		enabled = false;
	}
	
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	public void setOnArray(boolean onArray) {
		this.onArray = onArray;
	}
	
	public Module getModuleByName(String name) {
		Module module = null;
		for (Module module2 : Falcon2.getModuleManager().getModules()) {
			if (module2.getName().toLowerCase().equals(name.toLowerCase())) {
				module = module2;
				break;
			}
		}
		return module;
	}
}

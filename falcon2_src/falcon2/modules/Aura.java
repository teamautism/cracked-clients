package falcon2.modules;

import org.lwjgl.input.Keyboard;

import falcon2.Falcon2;
import falcon2.modules.commands.Command;
import falcon2.modules.commands.CommandManager;
import falcon2.modules.events.*;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;

public class Aura extends Module {
	public static float reach = 3.8F;
	public static Entity target = null;
	private static Long last = null, delay = 125L;
	
	public Aura() {
		super("Aura", Keyboard.KEY_R, Category.COMBAT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) { 
				handleAura();
			}
		}
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("ar") {
			@Override
			public boolean runCommand(String arguments) {
				if (arguments.contains(" ")) {
					String args[] = arguments.split(" ");
					if (args[0].equalsIgnoreCase("aps")) {
						delay = 1000L / (Long.parseLong(args[1]));
						addChatMessage("Aura aps set to '\247e" + args[1] + "\247f'.");
						return true;
					} else if (args[0].equalsIgnoreCase("reach")) {
						reach = Float.parseFloat(args[1]);
						addChatMessage("Aura reach set to '\247e" + reach + "\247f'.");
						return true;
					}
				}
				return false;
			}
		});
	}
	
	@Override
	public void onEnabled() {
		target = null;
		super.onEnabled();
	}
	
	private void handleAura() {
		if (target == null)
			return;
		if (!(last == null || getSysTime() >= last + delay))
			return;
		if (!AimBot.isFacingEntity(target))
			return;
		for (int i = 0; i < 1; i++) {
			getPlayer().inventory.currentItem = 0;
			getPlayer().swingItem();
			Falcon2.getModuleManager().callEvent(new EventAuraAttack(State.PRE));
			getController().attackEntity(getPlayer(), target);
			Falcon2.getModuleManager().callEvent(new EventAuraAttack(State.POST));
			last = getSysTime();
		}
	}
}

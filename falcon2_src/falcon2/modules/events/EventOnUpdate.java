package falcon2.modules.events;

public class EventOnUpdate extends EventCancel {
	private State state;
	
	public EventOnUpdate(State state) {
		this.state = state;
	}
	
	public State getState() {
		return state;
	}
}

package falcon2.modules.events;

public class EventBrightness extends Event {
	private float brightness;
	
	public float getBrightness() {
		return brightness;
	}
	
	public void setBrightness(float brightness) {
		this.brightness = brightness;
	}
}

package falcon2.modules.events;

public class EventMotionUpdate extends EventCancel {
	private State state;
	
	public EventMotionUpdate(State state) {
		this.state = state;
	}
	
	public State getState() {
		return state;
	}
}

package falcon2.modules.events;

import net.minecraft.entity.Entity;

public class EventVelocity extends Event {
	private Entity entity;
	private boolean canceled;
	
	public EventVelocity(Entity entity) {
		this.entity = entity;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
	public boolean getCanceled() {
		return canceled;
	}
	
	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
}

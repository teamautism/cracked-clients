package falcon2.modules.events;

public class EventPlayerSpeed extends Event {
	private float speed = 1.0F;

	public float getSpeed() {
		return speed;
	}
	
	public void setSpeed(float speed) {
		this.speed = speed;
	}
}

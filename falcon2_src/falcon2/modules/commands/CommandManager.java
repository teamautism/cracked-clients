package falcon2.modules.commands;

import java.util.ArrayList;
import falcon2.Wrapper;

public class CommandManager extends Wrapper {
	private static ArrayList<Command> commands = new ArrayList<Command>();
	private static String prefix = ".";
	
	public static ArrayList<Command> getCommands() {
		return commands;
	}
	
	public void callCommand(String s) {
		if (s.startsWith(prefix)) {
			s = s.substring(1).trim();
			boolean commandFound = false;
			for (Command command : getCommands()) {
				String s2 = "";
				String s3 = "";
				if (s.contains(" ")) {
					s2 = s.split(" ")[0].trim();
					s3 = s.substring(s.split(" ")[0].trim().length(), s.length()).trim();
				} else {
					s2 = s.trim();
				}
				if (command.getCommand().equalsIgnoreCase(s2)) {
					try {
						boolean cmdFound = command.runCommand(s3);
						if (cmdFound) {
							commandFound = true;
							break;
						}
					} catch (Exception e) {
						addChatMessageError("Failed to execute command.");
						commandFound = true;
						break;
					}
				}
			}
			if (!commandFound)
				addChatMessageError("Command not found.");
		} else {
			sendChatMessage(s);
		}
	}
}

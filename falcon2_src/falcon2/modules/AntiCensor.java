package falcon2.modules;

import falcon2.modules.events.*;

public class AntiCensor extends Module {
	public AntiCensor() {
		super("AntiCensor", null, Category.OTHER, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventMessage) {
			EventMessage event2 = (EventMessage) event;
			String s = event2.getMessage();
			if (s.startsWith(".") || s.startsWith("/"))
				return;
			try {
				String[] words = {
					"ass",
					"bitch",
					"cunt",
					"damn",
					"dick",
					"faggot",
					"fuck",
					"hell",
					"nigga",
					"nigger",
					"shit",
					"tits"
				};
				for (String word : words) {
					try {
						for (int i = 0; i < s.length(); i++) {
							String s2 = s.substring(i, i + word.length());
							if (s2.equalsIgnoreCase(word)) {
								String s3 = "";
								for (int i2 = 0; i2 < word.length() - 1; i2++)
									s3 += "*";
								s = s.replaceAll(s2, s2.substring(0, 1) + s3);
							}
						}
					} catch (Exception e) {}
				}
			} catch (Exception e) {}
			event2.setMessage(s);
		}
	}
}

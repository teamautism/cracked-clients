package falcon2.modules;

public enum Category {
	AURA, COMBAT, MOVEMENT, OTHER, RENDER, VALUE, WORLD;
	
	public static int getColor(Category category) {
		int color = 0xffffffff;
		if (category == Category.AURA)
			color = 0xffFF5555;
		else if (category == Category.COMBAT)
			color = 0xffAA0000;
		else if (category == Category.MOVEMENT)
			color = 0xffFFEA00;
		else if (category == Category.OTHER)
			color = 0xffFFFF55;
		else if (category == Category.RENDER)
			color = 0xff55FFFF;
		else if (category == Category.WORLD)
			color = 0xff55FF55;
		return color;
	}
	
	public static String fixCapitalization(String s) {
		if (s.length() >= 2) {
			String firstChar = s.substring(0, 1).toUpperCase();
			String restOfChars = s.substring(1).toLowerCase();
			return firstChar + restOfChars;
		} else {
			return s;
		}
	}
}

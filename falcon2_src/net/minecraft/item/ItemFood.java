package net.minecraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemFood extends Item {
   public final int itemUseDuration;
   private final int healAmount;
   private final float saturationModifier;
   private final boolean isWolfsFavoriteMeat;
   private boolean alwaysEdible;
   private int potionId;
   private int potionDuration;
   private int potionAmplifier;
   private float potionEffectProbability;
   private static final String __OBFID = "CL_00000036";

   public ItemFood(int p_i45339_1_, float p_i45339_2_, boolean p_i45339_3_) {
      this.itemUseDuration = 32;
      this.healAmount = p_i45339_1_;
      this.isWolfsFavoriteMeat = p_i45339_3_;
      this.saturationModifier = p_i45339_2_;
      this.setCreativeTab(CreativeTabs.tabFood);
   }

   public ItemFood(int p_i45340_1_, boolean p_i45340_2_) {
      this(p_i45340_1_, 0.6F, p_i45340_2_);
   }

   public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
      --par1ItemStack.stackSize;
      par3EntityPlayer.getFoodStats().func_151686_a(this, par1ItemStack);
      par2World.playSoundAtEntity(par3EntityPlayer, "random.burp", 0.5F, par2World.rand.nextFloat() * 0.1F + 0.9F);
      this.onFoodEaten(par1ItemStack, par2World, par3EntityPlayer);
      return par1ItemStack;
   }

   protected void onFoodEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
      if(!par2World.isClient && this.potionId > 0 && par2World.rand.nextFloat() < this.potionEffectProbability) {
         par3EntityPlayer.addPotionEffect(new PotionEffect(this.potionId, this.potionDuration * 20, this.potionAmplifier));
      }
   }

   public int getMaxItemUseDuration(ItemStack par1ItemStack) {
      return 32;
   }

   public EnumAction getItemUseAction(ItemStack par1ItemStack) {
      return EnumAction.eat;
   }

   public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
      if(par3EntityPlayer.canEat(this.alwaysEdible)) {
         par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
      }

      return par1ItemStack;
   }

   public int func_150905_g(ItemStack p_150905_1_) {
      return this.healAmount;
   }

   public float func_150906_h(ItemStack p_150906_1_) {
      return this.saturationModifier;
   }

   public boolean isWolfsFavoriteMeat() {
      return this.isWolfsFavoriteMeat;
   }

   public ItemFood setPotionEffect(int par1, int par2, int par3, float par4) {
      this.potionId = par1;
      this.potionDuration = par2;
      this.potionAmplifier = par3;
      this.potionEffectProbability = par4;
      return this;
   }

   public ItemFood setAlwaysEdible() {
      this.alwaysEdible = true;
      return this;
   }
}

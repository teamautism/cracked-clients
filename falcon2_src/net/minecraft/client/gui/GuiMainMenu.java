package net.minecraft.client.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import net.minecraft.client.Minecraft;
import net.minecraft.client.mco.ExceptionMcoService;
import net.minecraft.client.mco.ExceptionRetryCall;
import net.minecraft.client.mco.GuiScreenClientOutdated;
import net.minecraft.client.mco.McoClient;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Session;
import net.minecraft.world.demo.DemoWorldServer;
import net.minecraft.world.storage.ISaveFormat;
import net.minecraft.world.storage.WorldInfo;
import org.apache.commons.io.Charsets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Project;
import falcon2.gui.GuiAccountLogin;

public class GuiMainMenu extends GuiScreen {
	private float updateCounter;
	private String splashText;
	private int panoramaTimer;
	private DynamicTexture viewportTexture;
	private static final ResourceLocation splashTexts = new ResourceLocation("texts/splashes.txt");
	private static final ResourceLocation minecraftTitleTextures = new ResourceLocation("textures/gui/title/minecraft.png");
	private static final ResourceLocation[] titlePanoramaPaths = new ResourceLocation[]{new ResourceLocation("textures/gui/title/background/panorama_0.png"), new ResourceLocation("textures/gui/title/background/panorama_1.png"), new ResourceLocation("textures/gui/title/background/panorama_2.png"), new ResourceLocation("textures/gui/title/background/panorama_3.png"), new ResourceLocation("textures/gui/title/background/panorama_4.png"), new ResourceLocation("textures/gui/title/background/panorama_5.png")};
	private ResourceLocation field_110351_G;

	public void updateScreen() {
		panoramaTimer++;
	}

	public boolean doesGuiPauseGame() {
		return false;
	}

	protected void keyTyped(char par1, int par2) {}

	public void initGui() {
		viewportTexture = new DynamicTexture(256, 256);
		field_110351_G = mc.getTextureManager().getDynamicTextureLocation("background", viewportTexture);
		int var3 = height / 4 + 48;
		buttonList.add(new GuiButton(1, width / 2 - 100, var3, I18n.format("menu.singleplayer", new Object[0])));
		buttonList.add(new GuiButton(2, width / 2 - 100, var3 + (24 * 1), I18n.format("menu.multiplayer", new Object[0])));
		buttonList.add(new GuiButton(3, width / 2 - 100, var3 + 72 + 12, 98, 20, I18n.format("menu.options", new Object[0])));
		buttonList.add(new GuiButton(4, width / 2 + 2, var3 + 72 + 12, 98, 20, I18n.format("menu.quit", new Object[0])));
		buttonList.add(new GuiButtonLanguage(5, width / 2 - 124, var3 + 72 + 12));
	}

	protected void actionPerformed(GuiButton guiButton) {
		switch(guiButton.id) {
		case 1:
			mc.displayGuiScreen(new GuiSelectWorld(this));
			break;
		case 2:
			mc.displayGuiScreen(new GuiMultiplayer(this));
			break;
		case 3:
			mc.displayGuiScreen(new GuiOptions(this, mc.gameSettings));
			break;
		case 4:
			mc.shutdown();
			break;
		case 5:
			mc.displayGuiScreen(new GuiLanguage(this, mc.gameSettings, mc.getLanguageManager()));
			break;
		}
	}

	private void drawPanorama(int par1, int par2, float par3) {
		Tessellator var4 = Tessellator.instance;
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		Project.gluPerspective(120.0F, 1.0F, 0.05F, 10.0F);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
		GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		byte var5 = 8;
		for (int var6 = 0; var6 < var5 * var5; ++var6) {
			GL11.glPushMatrix();
			float var7 = ((float)(var6 % var5) / (float)var5 - 0.5F) / 64.0F;
			float var8 = ((float)(var6 / var5) / (float)var5 - 0.5F) / 64.0F;
			float var9 = 0.0F;
			GL11.glTranslatef(var7, var8, var9);
			GL11.glRotatef(MathHelper.sin(((float)panoramaTimer + par3) / 400.0F) * 25.0F + 20.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(-((float)panoramaTimer + par3) * 0.1F, 0.0F, 1.0F, 0.0F);
			for (int var10 = 0; var10 < 6; ++var10) {
				GL11.glPushMatrix();
				if(var10 == 1)
					GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
				if(var10 == 2)
					GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
				if(var10 == 3)
					GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
				if(var10 == 4)
					GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
				if(var10 == 5)
					GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
				mc.getTextureManager().bindTexture(titlePanoramaPaths[var10]);
				var4.startDrawingQuads();
				var4.setColorRGBA_I(16777215, 255 / (var6 + 1));
				float var11 = 0.0F;
				var4.addVertexWithUV(-1.0D, -1.0D, 1.0D, (double)(0.0F + var11), (double)(0.0F + var11));
				var4.addVertexWithUV(1.0D, -1.0D, 1.0D, (double)(1.0F - var11), (double)(0.0F + var11));
				var4.addVertexWithUV(1.0D, 1.0D, 1.0D, (double)(1.0F - var11), (double)(1.0F - var11));
				var4.addVertexWithUV(-1.0D, 1.0D, 1.0D, (double)(0.0F + var11), (double)(1.0F - var11));
				var4.draw();
				GL11.glPopMatrix();
			}
			GL11.glPopMatrix();
			GL11.glColorMask(true, true, true, false);
		}
		var4.setTranslation(0.0D, 0.0D, 0.0D);
		GL11.glColorMask(true, true, true, true);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPopMatrix();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	private void rotateAndBlurSkybox(float par1) {
		mc.getTextureManager().bindTexture(field_110351_G);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glCopyTexSubImage2D(GL11.GL_TEXTURE_2D, 0, 0, 0, 0, 0, 256, 256);
		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glColorMask(true, true, true, false);
		Tessellator var2 = Tessellator.instance;
		var2.startDrawingQuads();
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		byte var3 = 3;
		for (int var4 = 0; var4 < var3; ++var4) {
			var2.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F / (float)(var4 + 1));
			int var5 = width;
			int var6 = height;
			float var7 = (float)(var4 - var3 / 2) / 256.0F;
			var2.addVertexWithUV((double)var5, (double)var6, (double)zLevel, (double)(0.0F + var7), 1.0D);
			var2.addVertexWithUV((double)var5, 0.0D, (double)zLevel, (double)(1.0F + var7), 1.0D);
			var2.addVertexWithUV(0.0D, 0.0D, (double)zLevel, (double)(1.0F + var7), 0.0D);
			var2.addVertexWithUV(0.0D, (double)var6, (double)zLevel, (double)(0.0F + var7), 0.0D);
		}
		var2.draw();
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glColorMask(true, true, true, true);
	}

	private void renderSkybox(int par1, int par2, float par3) {
		mc.getFramebuffer().unbindFramebuffer();
		GL11.glViewport(0, 0, 256, 256);
		drawPanorama(par1, par2, par3);
		rotateAndBlurSkybox(par3);
		rotateAndBlurSkybox(par3);
		rotateAndBlurSkybox(par3);
		rotateAndBlurSkybox(par3);
		rotateAndBlurSkybox(par3);
		rotateAndBlurSkybox(par3);
		rotateAndBlurSkybox(par3);
		mc.getFramebuffer().bindFramebuffer(true);
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		Tessellator var4 = Tessellator.instance;
		var4.startDrawingQuads();
		float var5 = width > height?120.0F / (float)width:120.0F / (float)height;
		float var6 = (float)height * var5 / 256.0F;
		float var7 = (float)width * var5 / 256.0F;
		var4.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
		int var8 = width;
		int var9 = height;
		var4.addVertexWithUV(0.0D, (double)var9, (double)zLevel, (double)(0.5F - var6), (double)(0.5F + var7));
		var4.addVertexWithUV((double)var8, (double)var9, (double)zLevel, (double)(0.5F - var6), (double)(0.5F - var7));
		var4.addVertexWithUV((double)var8, 0.0D, (double)zLevel, (double)(0.5F + var6), (double)(0.5F - var7));
		var4.addVertexWithUV(0.0D, 0.0D, (double)zLevel, (double)(0.5F + var6), (double)(0.5F + var7));
		var4.draw();
	}

	public void drawScreen(int par1, int par2, float par3) {
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		renderSkybox(par1, par2, par3);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		Tessellator var4 = Tessellator.instance;
		short var5 = 274;
		int var6 = width / 2 - var5 / 2;
		byte var7 = 30;
		drawGradientRect(0, 0, width, height, -2130706433, 16777215);
		drawGradientRect(0, 0, width, height, 0, Integer.MIN_VALUE);
		mc.getTextureManager().bindTexture(minecraftTitleTextures);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		drawTexturedModalRect(var6 + 0, var7 + 0, 0, 0, 155, 44);
		drawTexturedModalRect(var6 + 155, var7 + 0, 0, 45, 155, 44);
		var4.setColorOpaque_I(-1);
		GL11.glPushMatrix();
		GL11.glTranslatef((float)(width / 2 + 90), 70.0F, 0.0F);
		GL11.glRotatef(-20.0F, 0.0F, 0.0F, 1.0F);
		float var8 = 1.8F - MathHelper.abs(MathHelper.sin((float)(Minecraft.getSystemTime() % 1000L) / 1000.0F * (float)Math.PI * 2.0F) * 0.1F);
		var8 = var8 * 100.0F / (float)(fontRendererObj.getStringWidth(splashText) + 32);
		GL11.glScalef(var8, var8, var8);
		drawCenteredString(fontRendererObj, splashText, 0, -8, -256);
		GL11.glPopMatrix();
		String var9 = "Minecraft 1.7.2";
		drawString(fontRendererObj, var9, 2, height - 10, -1);
		String var10 = "Copyright Mojang AB. Do not distribute!";
		drawString(fontRendererObj, var10, width - fontRendererObj.getStringWidth(var10) - 2, height - 10, -1);
		super.drawScreen(par1, par2, par3);
	}
}

package caillou.modyoulhecks.nkrypt;

public class ModExample extends Module
{
    public ModExample()
    {
        super("Fullbright", 35);
    }

    public void onEnabled()
    {
        this.sendChatMessage("Enabled!");
    }

    public void onDisabled()
    {
        this.sendChatMessage("Disabled!");
    }
}

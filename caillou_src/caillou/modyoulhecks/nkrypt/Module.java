package caillou.modyoulhecks.nkrypt;

import net.minecraft.src.Minecraft;
import net.minecraft.src.Packet3Chat;

public class Module
{
    private String name;
    private int key;
    private boolean enabled;

    public Minecraft getMinecraft()
    {
        return Minecraft.getMinecraft();
    }

    public void sendChatMessage(String s)
    {
        this.getMinecraft().thePlayer.sendQueue.addToSendQueue(new Packet3Chat(s));
    }

    public void addChatMessage(String s)
    {
        this.getMinecraft().thePlayer.addChatMessage("[Client] " + s);
    }

    public void onCommand(String[] s)
    {
        this.addChatMessage("This Module has no commands setup.");
    }

    public void onTick() {}

    public Module(String name, int key)
    {
        this.name = name;
        this.key = key;
    }

    public void onEnabled() {}

    public void onDisabled() {}

    public String getName()
    {
        return this.name;
    }

    public int getKey()
    {
        return this.key;
    }

    public void setName(String s)
    {
        this.name = s;
    }

    public void setKey(int i)
    {
        this.key = i;
    }

    public boolean isEnabled()
    {
        return this.enabled;
    }

    public void setEnabled(boolean flag)
    {
        this.enabled = flag;
    }

    public void toggle()
    {
        this.setEnabled(!this.isEnabled());

        if (this.isEnabled())
        {
            this.onEnabled();
        }
        else
        {
            this.onDisabled();
        }
    }
}

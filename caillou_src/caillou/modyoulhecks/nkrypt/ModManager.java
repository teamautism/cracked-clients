package caillou.modyoulhecks.nkrypt;

public class ModManager
{
    public static Module[] mods = new Module[] {new ModExample()};

    public static Module getModByName(String name)
    {
        Module[] arr$ = mods;
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$)
        {
            Module mod = arr$[i$];

            if (mod.getName().equalsIgnoreCase(name))
            {
                return mod;
            }
        }

        return null;
    }
}

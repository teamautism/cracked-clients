package net.minecraft.src;

import java.awt.Color;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class GuiIngame extends Gui
{
    private static final ResourceLocation field_110329_b = new ResourceLocation("textures/misc/vignette.png");
    private static final ResourceLocation field_110330_c = new ResourceLocation("textures/gui/widgets.png");
    private static final ResourceLocation field_110328_d = new ResourceLocation("textures/misc/pumpkinblur.png");
    private static final RenderItem itemRenderer = new RenderItem();
    private final Random rand = new Random();
    private final Minecraft mc;

    /** ChatGUI instance that retains all previous chat data */
    private final GuiNewChat persistantChatGUI;
    private int updateCounter;

    /** The string specifying which record music is playing */
    private String recordPlaying = "";

    /** How many ticks the record playing message will be displayed */
    private int recordPlayingUpFor;
    private boolean recordIsPlaying;

    /** Previous frame vignette brightness (slowly changes by 1% each frame) */
    public float prevVignetteBrightness = 1.0F;

    /** Remaining ticks the item highlight should be visible */
    private int remainingHighlightTicks;

    /** The ItemStack that is currently being highlighted */
    private ItemStack highlightingItemStack;
    public static boolean nopush;
    public static boolean tracer;
    public static boolean radar;
    public static boolean mobaura;
    public static boolean killaura;
    public static boolean animalaura;
    public static boolean crits;
    public static boolean sprint;
    public static boolean fastmine;
    public static boolean direct = false;
    public static boolean heck = true;
    public static boolean gui = true;
    private boolean[] keyStates = new boolean[256];

    public GuiIngame(Minecraft par1Minecraft)
    {
        this.mc = par1Minecraft;
        this.persistantChatGUI = new GuiNewChat(par1Minecraft);
    }

    private boolean checkKey(int i)
    {
        return this.mc.currentScreen != null ? false : (Keyboard.isKeyDown(i) != this.keyStates[i] ? (this.keyStates[i] = !this.keyStates[i]) : false);
    }

    /**
     * Render the ingame overlay with quick icon bar, ...
     */
    public void renderGameOverlay(float par1, boolean par2, int par3, int par4)
    {
        ScaledResolution var5 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
        int var6 = var5.getScaledWidth();
        int var7 = var5.getScaledHeight();
        FontRenderer var8 = this.mc.fontRenderer;
        this.mc.entityRenderer.setupOverlayRendering();
        GL11.glEnable(GL11.GL_BLEND);

        if (Minecraft.isFancyGraphicsEnabled())
        {
            this.renderVignette(this.mc.thePlayer.getBrightness(par1), var6, var7);
        }
        else
        {
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        }

        ItemStack var9 = this.mc.thePlayer.inventory.armorItemInSlot(3);

        if (this.mc.gameSettings.thirdPersonView == 0 && var9 != null && var9.itemID == Block.pumpkin.blockID)
        {
            this.renderPumpkinBlur(var6, var7);
        }

        if (!this.mc.thePlayer.isPotionActive(Potion.confusion))
        {
            float var10 = this.mc.thePlayer.prevTimeInPortal + (this.mc.thePlayer.timeInPortal - this.mc.thePlayer.prevTimeInPortal) * par1;

            if (var10 > 0.0F)
            {
                this.func_130015_b(var10, var6, var7);
            }
        }

        int var11;
        int var12;
        int var37;

        if (!this.mc.playerController.enableEverythingIsScrewedUpMode())
        {
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.mc.func_110434_K().func_110577_a(field_110330_c);
            InventoryPlayer var13 = this.mc.thePlayer.inventory;
            this.zLevel = -90.0F;
            this.drawTexturedModalRect(var6 / 2 - 91, var7 - 22, 0, 0, 182, 22);
            this.drawTexturedModalRect(var6 / 2 - 91 - 1 + var13.currentItem * 20, var7 - 22 - 1, 0, 22, 24, 22);
            this.mc.func_110434_K().func_110577_a(field_110324_m);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_COLOR, GL11.GL_ONE_MINUS_SRC_COLOR);
            this.drawTexturedModalRect(var6 / 2 - 7, var7 / 2 - 7, 0, 0, 16, 16);
            GL11.glDisable(GL11.GL_BLEND);
            this.mc.mcProfiler.startSection("bossHealth");
            this.renderBossHealth();
            this.mc.mcProfiler.endSection();

            if (this.mc.playerController.shouldDrawHUD())
            {
                this.func_110327_a(var6, var7);
            }

            GL11.glDisable(GL11.GL_BLEND);
            this.mc.mcProfiler.startSection("actionBar");
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.enableGUIStandardItemLighting();

            for (var37 = 0; var37 < 9; ++var37)
            {
                var11 = var6 / 2 - 90 + var37 * 20 + 2;
                var12 = var7 - 16 - 3;
                this.renderInventorySlot(var37, var11, var12, par1);
            }

            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            this.mc.mcProfiler.endSection();
        }

        int var38;

        if (this.mc.thePlayer.getSleepTimer() > 0)
        {
            this.mc.mcProfiler.startSection("sleep");
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glDisable(GL11.GL_ALPHA_TEST);
            var38 = this.mc.thePlayer.getSleepTimer();
            float var14 = (float)var38 / 100.0F;

            if (var14 > 1.0F)
            {
                var14 = 1.0F - (float)(var38 - 100) / 10.0F;
            }

            var11 = (int)(220.0F * var14) << 24 | 1052704;
            drawRect(0, 0, var6, var7, var11);
            GL11.glEnable(GL11.GL_ALPHA_TEST);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            this.mc.mcProfiler.endSection();
        }

        var38 = 16777215;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        var37 = var6 / 2 - 91;
        int var15;
        int var17;
        int var16;
        short var19;
        float var18;
        int var39;

        if (this.mc.thePlayer.func_110317_t())
        {
            this.mc.mcProfiler.startSection("jumpBar");
            this.mc.func_110434_K().func_110577_a(Gui.field_110324_m);
            var18 = this.mc.thePlayer.func_110319_bJ();
            var19 = 182;
            var39 = (int)(var18 * (float)(var19 + 1));
            var15 = var7 - 32 + 3;
            this.drawTexturedModalRect(var37, var15, 0, 84, var19, 5);

            if (var39 > 0)
            {
                this.drawTexturedModalRect(var37, var15, 0, 89, var39, 5);
            }

            this.mc.mcProfiler.endSection();
        }
        else if (this.mc.playerController.func_78763_f())
        {
            this.mc.mcProfiler.startSection("expBar");
            this.mc.func_110434_K().func_110577_a(Gui.field_110324_m);
            var11 = this.mc.thePlayer.xpBarCap();

            if (var11 > 0)
            {
                var19 = 182;
                var39 = (int)(this.mc.thePlayer.experience * (float)(var19 + 1));
                var15 = var7 - 32 + 3;
                this.drawTexturedModalRect(var37, var15, 0, 64, var19, 5);

                if (var39 > 0)
                {
                    this.drawTexturedModalRect(var37, var15, 0, 69, var39, 5);
                }
            }

            this.mc.mcProfiler.endSection();

            if (this.mc.thePlayer.experienceLevel > 0)
            {
                this.mc.mcProfiler.startSection("expLevel");
                boolean var20 = false;
                var39 = var20 ? 16777215 : 8453920;
                String var21 = "" + this.mc.thePlayer.experienceLevel;
                var17 = (var6 - var8.getStringWidth(var21)) / 2;
                var16 = var7 - 31 - 4;
                boolean var22 = false;
                var8.drawString(var21, var17 + 1, var16, 0);
                var8.drawString(var21, var17 - 1, var16, 0);
                var8.drawString(var21, var17, var16 + 1, 0);
                var8.drawString(var21, var17, var16 - 1, 0);
                var8.drawString(var21, var17, var16, var39);
                this.mc.mcProfiler.endSection();
            }
        }

        String var40;

        if (this.mc.gameSettings.heldItemTooltips)
        {
            this.mc.mcProfiler.startSection("toolHighlight");

            if (this.remainingHighlightTicks > 0 && this.highlightingItemStack != null)
            {
                var40 = this.highlightingItemStack.getDisplayName();
                var12 = (var6 - var8.getStringWidth(var40)) / 2;
                var39 = var7 - 59;

                if (!this.mc.playerController.shouldDrawHUD())
                {
                    var39 += 14;
                }

                var15 = (int)((float)this.remainingHighlightTicks * 256.0F / 10.0F);

                if (var15 > 255)
                {
                    var15 = 255;
                }

                if (var15 > 0)
                {
                    GL11.glPushMatrix();
                    GL11.glEnable(GL11.GL_BLEND);
                    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                    var8.drawStringWithShadow(var40, var12, var39, 16777215 + (var15 << 24));
                    GL11.glDisable(GL11.GL_BLEND);
                    GL11.glPopMatrix();
                }
            }

            this.mc.mcProfiler.endSection();
        }

        if (this.mc.isDemo())
        {
            this.mc.mcProfiler.startSection("demo");
            var40 = "";

            if (this.mc.theWorld.getTotalWorldTime() >= 120500L)
            {
                var40 = I18n.func_135053_a("demo.demoExpired");
            }
            else
            {
                var40 = I18n.func_135052_a("demo.remainingTime", new Object[] {StringUtils.ticksToElapsedTime((int)(120500L - this.mc.theWorld.getTotalWorldTime()))});
            }

            var12 = var8.getStringWidth(var40);
            var8.drawStringWithShadow(var40, var6 - var12 - 10, 5, 16777215);
            this.mc.mcProfiler.endSection();
        }

        int var23;
        int var34;
        String var32;
        int var33;
        int var42;
        int var41;

        if (this.mc.gameSettings.showDebugInfo)
        {
            this.mc.mcProfiler.startSection("debug");
            GL11.glPushMatrix();
            var8.drawStringWithShadow("Minecraft 1.6.2 (" + this.mc.debug + ")", 2, 2, 16777215);
            var8.drawStringWithShadow(this.mc.debugInfoRenders(), 2, 12, 16777215);
            var8.drawStringWithShadow(this.mc.getEntityDebug(), 2, 22, 16777215);
            var8.drawStringWithShadow(this.mc.debugInfoEntities(), 2, 32, 16777215);
            var8.drawStringWithShadow(this.mc.getWorldProviderName(), 2, 42, 16777215);
            long var24 = Runtime.getRuntime().maxMemory();
            long var26 = Runtime.getRuntime().totalMemory();
            long var28 = Runtime.getRuntime().freeMemory();
            long var30 = var26 - var28;
            var32 = "Used memory: " + var30 * 100L / var24 + "% (" + var30 / 1024L / 1024L + "MB) of " + var24 / 1024L / 1024L + "MB";
            var42 = 14737632;
            this.drawString(var8, var32, var6 - var8.getStringWidth(var32) - 2, 2, 14737632);
            var32 = "Allocated memory: " + var26 * 100L / var24 + "% (" + var26 / 1024L / 1024L + "MB)";
            this.drawString(var8, var32, var6 - var8.getStringWidth(var32) - 2, 12, 14737632);
            var23 = MathHelper.floor_double(this.mc.thePlayer.posX);
            var41 = MathHelper.floor_double(this.mc.thePlayer.posY);
            var33 = MathHelper.floor_double(this.mc.thePlayer.posZ);
            this.drawString(var8, String.format("x: %.5f (%d) // c: %d (%d)", new Object[] {Double.valueOf(this.mc.thePlayer.posX), Integer.valueOf(var23), Integer.valueOf(var23 >> 4), Integer.valueOf(var23 & 15)}), 2, 64, 14737632);
            this.drawString(var8, String.format("y: %.3f (feet pos, %.3f eyes pos)", new Object[] {Double.valueOf(this.mc.thePlayer.boundingBox.minY), Double.valueOf(this.mc.thePlayer.posY)}), 2, 72, 14737632);
            this.drawString(var8, String.format("z: %.5f (%d) // c: %d (%d)", new Object[] {Double.valueOf(this.mc.thePlayer.posZ), Integer.valueOf(var33), Integer.valueOf(var33 >> 4), Integer.valueOf(var33 & 15)}), 2, 80, 14737632);
            var34 = MathHelper.floor_double((double)(this.mc.thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
            this.drawString(var8, "f: " + var34 + " (" + Direction.directions[var34] + ") / " + MathHelper.wrapAngleTo180_float(this.mc.thePlayer.rotationYaw), 2, 88, 14737632);

            if (this.mc.theWorld != null && this.mc.theWorld.blockExists(var23, var41, var33))
            {
                Chunk var35 = this.mc.theWorld.getChunkFromBlockCoords(var23, var33);
                this.drawString(var8, "lc: " + (var35.getTopFilledSegment() + 15) + " b: " + var35.getBiomeGenForWorldCoords(var23 & 15, var33 & 15, this.mc.theWorld.getWorldChunkManager()).biomeName + " bl: " + var35.getSavedLightValue(EnumSkyBlock.Block, var23 & 15, var41, var33 & 15) + " sl: " + var35.getSavedLightValue(EnumSkyBlock.Sky, var23 & 15, var41, var33 & 15) + " rl: " + var35.getBlockLightValue(var23 & 15, var41, var33 & 15, 0), 2, 96, 14737632);
            }

            this.drawString(var8, String.format("ws: %.3f, fs: %.3f, g: %b, fl: %d", new Object[] {Float.valueOf(this.mc.thePlayer.capabilities.getWalkSpeed()), Float.valueOf(this.mc.thePlayer.capabilities.getFlySpeed()), Boolean.valueOf(this.mc.thePlayer.onGround), Integer.valueOf(this.mc.theWorld.getHeightValue(var23, var33))}), 2, 104, 14737632);
            GL11.glPopMatrix();
            this.mc.mcProfiler.endSection();
        }

        if (gui)
        {
            this.drawBorderedRect(0, 0, 100, 70, 1, -12829636, -10724260);
            int var43 = (int)((double)this.mc.thePlayer.ticksExisted * 1.4D);

            if (var43 < 9000)
            {
                if (heck)
                {
                    var8.drawLargeStringWithShadow("\u00a7l\u00a7nCaillou 1.0", var43 - 93, 3, 16777215);
                }
                else
                {
                    var8.drawLargeStringWithShadow("\u00a7l\u00a7nCaillou 1.0", var43 - 93, 3, 16777215);
                }
            }

            if (tracer)
            {
                var8.drawString("\u00a7alinehake", 2, 12, 16737792);
            }
            else
            {
                var8.drawString("\u00a74linehake", 2, 12, 10066329);
            }

            if (killaura)
            {
                var8.drawString("\u00a7aawto atak", 2, 22, 16737792);
            }
            else
            {
                var8.drawString("\u00a74awto atak", 2, 22, 10066329);
            }

            if (sprint)
            {
                var8.drawString("\u00a7ausanebolte", 2, 32, 16737792);
            }
            else
            {
                var8.drawString("\u00a74usanebolte", 2, 32, 10066329);
            }

            if (fastmine)
            {
                var8.drawString("\u00a7aspready gunzalezbian", 2, 42, 16737792);
            }
            else
            {
                var8.drawString("\u00a74spready gunzalezbian", 2, 42, 10066329);
            }
        }

        if (this.checkKey(49))
        {
            tracer = !tracer;
        }

        if (this.checkKey(19))
        {
            killaura = !killaura;
        }

        if (this.checkKey(37))
        {
            sprint = !sprint;
        }

        if (this.checkKey(50))
        {
            fastmine = !fastmine;
        }

        if (this.checkKey(200))
        {
            gui = !gui;
        }

        if (this.recordPlayingUpFor > 0)
        {
            this.mc.mcProfiler.startSection("overlayMessage");
            var18 = (float)this.recordPlayingUpFor - par1;
            var12 = (int)(var18 * 255.0F / 20.0F);

            if (var12 > 255)
            {
                var12 = 255;
            }

            if (var12 > 8)
            {
                GL11.glPushMatrix();
                GL11.glTranslatef((float)(var6 / 2), (float)(var7 - 68), 0.0F);
                GL11.glEnable(GL11.GL_BLEND);
                GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                var39 = 16777215;

                if (this.recordIsPlaying)
                {
                    var39 = Color.HSBtoRGB(var18 / 50.0F, 0.7F, 0.6F) & 16777215;
                }

                var8.drawString(this.recordPlaying, -var8.getStringWidth(this.recordPlaying) / 2, -4, var39 + (var12 << 24 & -16777216));
                GL11.glDisable(GL11.GL_BLEND);
                GL11.glPopMatrix();
            }

            this.mc.mcProfiler.endSection();
        }

        ScoreObjective var44 = this.mc.theWorld.getScoreboard().func_96539_a(1);

        if (var44 != null)
        {
            this.func_96136_a(var44, var7, var6, var8);
        }

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, (float)(var7 - 48), 0.0F);
        this.mc.mcProfiler.startSection("chat");
        this.persistantChatGUI.drawChat(this.updateCounter);
        this.mc.mcProfiler.endSection();
        GL11.glPopMatrix();
        var44 = this.mc.theWorld.getScoreboard().func_96539_a(0);

        if (this.mc.gameSettings.keyBindPlayerList.pressed && (!this.mc.isIntegratedServerRunning() || this.mc.thePlayer.sendQueue.playerInfoList.size() > 1 || var44 != null))
        {
            this.mc.mcProfiler.startSection("playerList");
            NetClientHandler var25 = this.mc.thePlayer.sendQueue;
            List var46 = var25.playerInfoList;
            var15 = var25.currentServerMaxPlayers;
            var17 = var15;

            for (var16 = 1; var17 > 20; var17 = (var15 + var16 - 1) / var16)
            {
                ++var16;
            }

            int var27 = 300 / var16;

            if (var27 > 150)
            {
                var27 = 150;
            }

            int var45 = (var6 - var16 * var27) / 2;
            byte var29 = 10;
            drawRect(var45 - 1, var29 - 1, var45 + var27 * var16, var29 + 9 * var17, Integer.MIN_VALUE);

            for (var42 = 0; var42 < var15; ++var42)
            {
                var23 = var45 + var42 % var16 * var27;
                var41 = var29 + var42 / var16 * 9;
                drawRect(var23, var41, var23 + var27 - 1, var41 + 8, 553648127);
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                GL11.glEnable(GL11.GL_ALPHA_TEST);

                if (var42 < var46.size())
                {
                    GuiPlayerInfo var47 = (GuiPlayerInfo)var46.get(var42);
                    ScorePlayerTeam var31 = this.mc.theWorld.getScoreboard().getPlayersTeam(var47.name);
                    var32 = ScorePlayerTeam.formatPlayerName(var31, var47.name);
                    var8.drawStringWithShadow(var32, var23, var41, 16777215);

                    if (var44 != null)
                    {
                        var33 = var23 + var8.getStringWidth(var32) + 5;
                        var34 = var23 + var27 - 12 - 5;

                        if (var34 - var33 > 5)
                        {
                            Score var50 = var44.getScoreboard().func_96529_a(var47.name, var44);
                            String var36 = EnumChatFormatting.YELLOW + "" + var50.getScorePoints();
                            var8.drawStringWithShadow(var36, var34 - var8.getStringWidth(var36), var41, 16777215);
                        }
                    }

                    GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                    this.mc.func_110434_K().func_110577_a(field_110324_m);
                    byte var48 = 0;
                    boolean var51 = false;
                    byte var49;

                    if (var47.responseTime < 0)
                    {
                        var49 = 5;
                    }
                    else if (var47.responseTime < 150)
                    {
                        var49 = 0;
                    }
                    else if (var47.responseTime < 300)
                    {
                        var49 = 1;
                    }
                    else if (var47.responseTime < 600)
                    {
                        var49 = 2;
                    }
                    else if (var47.responseTime < 1000)
                    {
                        var49 = 3;
                    }
                    else
                    {
                        var49 = 4;
                    }

                    this.zLevel += 100.0F;
                    this.drawTexturedModalRect(var23 + var27 - 12, var41, 0 + var48 * 10, 176 + var49 * 8, 10, 8);
                    this.zLevel -= 100.0F;
                }
            }
        }

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
    }

    private void func_96136_a(ScoreObjective par1ScoreObjective, int par2, int par3, FontRenderer par4FontRenderer)
    {
        Scoreboard var5 = par1ScoreObjective.getScoreboard();
        Collection var6 = var5.func_96534_i(par1ScoreObjective);

        if (var6.size() <= 15)
        {
            int var7 = par4FontRenderer.getStringWidth(par1ScoreObjective.getDisplayName());
            String var8;

            for (Iterator var9 = var6.iterator(); var9.hasNext(); var7 = Math.max(var7, par4FontRenderer.getStringWidth(var8)))
            {
                Score var10 = (Score)var9.next();
                ScorePlayerTeam var11 = var5.getPlayersTeam(var10.getPlayerName());
                var8 = ScorePlayerTeam.formatPlayerName(var11, var10.getPlayerName()) + ": " + EnumChatFormatting.RED + var10.getScorePoints();
            }

            int var22 = var6.size() * par4FontRenderer.FONT_HEIGHT;
            int var24 = par2 / 2 + var22 / 3;
            byte var23 = 3;
            int var12 = par3 - var7 - var23;
            int var13 = 0;
            Iterator var14 = var6.iterator();

            while (var14.hasNext())
            {
                Score var15 = (Score)var14.next();
                ++var13;
                ScorePlayerTeam var16 = var5.getPlayersTeam(var15.getPlayerName());
                String var17 = ScorePlayerTeam.formatPlayerName(var16, var15.getPlayerName());
                String var18 = EnumChatFormatting.RED + "" + var15.getScorePoints();
                int var19 = var24 - var13 * par4FontRenderer.FONT_HEIGHT;
                int var20 = par3 - var23 + 2;
                drawRect(var12 - 2, var19, var20, var19 + par4FontRenderer.FONT_HEIGHT, 1342177280);
                par4FontRenderer.drawString(var17, var12, var19, 553648127);
                par4FontRenderer.drawString(var18, var20 - par4FontRenderer.getStringWidth(var18), var19, 553648127);

                if (var13 == var6.size())
                {
                    String var21 = par1ScoreObjective.getDisplayName();
                    drawRect(var12 - 2, var19 - par4FontRenderer.FONT_HEIGHT - 1, var20, var19 - 1, 1610612736);
                    drawRect(var12 - 2, var19 - 1, var20, var19, 1342177280);
                    par4FontRenderer.drawString(var21, var12 + var7 / 2 - par4FontRenderer.getStringWidth(var21) / 2, var19 - par4FontRenderer.FONT_HEIGHT, 553648127);
                }
            }
        }
    }

    private void func_110327_a(int par1, int par2)
    {
        boolean var3 = this.mc.thePlayer.hurtResistantTime / 3 % 2 == 1;

        if (this.mc.thePlayer.hurtResistantTime < 10)
        {
            var3 = false;
        }

        int var4 = MathHelper.ceiling_float_int(this.mc.thePlayer.func_110143_aJ());
        int var5 = MathHelper.ceiling_float_int(this.mc.thePlayer.prevHealth);
        this.rand.setSeed((long)(this.updateCounter * 312871));
        boolean var6 = false;
        FoodStats var7 = this.mc.thePlayer.getFoodStats();
        int var8 = var7.getFoodLevel();
        int var9 = var7.getPrevFoodLevel();
        AttributeInstance var10 = this.mc.thePlayer.func_110148_a(SharedMonsterAttributes.field_111267_a);
        int var11 = par1 / 2 - 91;
        int var12 = par1 / 2 + 91;
        int var13 = par2 - 39;
        float var14 = (float)var10.func_111126_e();
        float var15 = this.mc.thePlayer.func_110139_bj();
        int var16 = MathHelper.ceiling_float_int((var14 + var15) / 2.0F / 10.0F);
        int var17 = Math.max(10 - (var16 - 2), 3);
        int var18 = var13 - (var16 - 1) * var17 - 10;
        float var19 = var15;
        int var20 = this.mc.thePlayer.getTotalArmorValue();
        int var21 = -1;

        if (this.mc.thePlayer.isPotionActive(Potion.regeneration))
        {
            var21 = this.updateCounter % MathHelper.ceiling_float_int(var14 + 5.0F);
        }

        this.mc.mcProfiler.startSection("armor");
        int var23;
        int var22;

        for (var23 = 0; var23 < 10; ++var23)
        {
            if (var20 > 0)
            {
                var22 = var11 + var23 * 8;

                if (var23 * 2 + 1 < var20)
                {
                    this.drawTexturedModalRect(var22, var18, 34, 9, 9, 9);
                }

                if (var23 * 2 + 1 == var20)
                {
                    this.drawTexturedModalRect(var22, var18, 25, 9, 9, 9);
                }

                if (var23 * 2 + 1 > var20)
                {
                    this.drawTexturedModalRect(var22, var18, 16, 9, 9, 9);
                }
            }
        }

        this.mc.mcProfiler.endStartSection("health");
        int var25;
        int var24;
        int var26;

        for (var23 = MathHelper.ceiling_float_int((var14 + var15) / 2.0F) - 1; var23 >= 0; --var23)
        {
            var22 = 16;

            if (this.mc.thePlayer.isPotionActive(Potion.poison))
            {
                var22 += 36;
            }
            else if (this.mc.thePlayer.isPotionActive(Potion.wither))
            {
                var22 += 72;
            }

            byte var27 = 0;

            if (var3)
            {
                var27 = 1;
            }

            var24 = MathHelper.ceiling_float_int((float)(var23 + 1) / 10.0F) - 1;
            var26 = var11 + var23 % 10 * 8;
            var25 = var13 - var24 * var17;

            if (var4 <= 4)
            {
                var25 += this.rand.nextInt(2);
            }

            if (var23 == var21)
            {
                var25 -= 2;
            }

            byte var28 = 0;

            if (this.mc.theWorld.getWorldInfo().isHardcoreModeEnabled())
            {
                var28 = 5;
            }

            this.drawTexturedModalRect(var26, var25, 16 + var27 * 9, 9 * var28, 9, 9);

            if (var3)
            {
                if (var23 * 2 + 1 < var5)
                {
                    this.drawTexturedModalRect(var26, var25, var22 + 54, 9 * var28, 9, 9);
                }

                if (var23 * 2 + 1 == var5)
                {
                    this.drawTexturedModalRect(var26, var25, var22 + 63, 9 * var28, 9, 9);
                }
            }

            if (var19 > 0.0F)
            {
                if (var19 == var15 && var15 % 2.0F == 1.0F)
                {
                    this.drawTexturedModalRect(var26, var25, var22 + 153, 9 * var28, 9, 9);
                }
                else
                {
                    this.drawTexturedModalRect(var26, var25, var22 + 144, 9 * var28, 9, 9);
                }

                var19 -= 2.0F;
            }
            else
            {
                if (var23 * 2 + 1 < var4)
                {
                    this.drawTexturedModalRect(var26, var25, var22 + 36, 9 * var28, 9, 9);
                }

                if (var23 * 2 + 1 == var4)
                {
                    this.drawTexturedModalRect(var26, var25, var22 + 45, 9 * var28, 9, 9);
                }
            }
        }

        Entity var37 = this.mc.thePlayer.ridingEntity;
        int var39;

        if (var37 == null)
        {
            this.mc.mcProfiler.endStartSection("food");

            for (var22 = 0; var22 < 10; ++var22)
            {
                var39 = var13;
                var24 = 16;
                byte var29 = 0;

                if (this.mc.thePlayer.isPotionActive(Potion.hunger))
                {
                    var24 += 36;
                    var29 = 13;
                }

                if (this.mc.thePlayer.getFoodStats().getSaturationLevel() <= 0.0F && this.updateCounter % (var8 * 3 + 1) == 0)
                {
                    var39 = var13 + (this.rand.nextInt(3) - 1);
                }

                if (var6)
                {
                    var29 = 1;
                }

                var25 = var12 - var22 * 8 - 9;
                this.drawTexturedModalRect(var25, var39, 16 + var29 * 9, 27, 9, 9);

                if (var6)
                {
                    if (var22 * 2 + 1 < var9)
                    {
                        this.drawTexturedModalRect(var25, var39, var24 + 54, 27, 9, 9);
                    }

                    if (var22 * 2 + 1 == var9)
                    {
                        this.drawTexturedModalRect(var25, var39, var24 + 63, 27, 9, 9);
                    }
                }

                if (var22 * 2 + 1 < var8)
                {
                    this.drawTexturedModalRect(var25, var39, var24 + 36, 27, 9, 9);
                }

                if (var22 * 2 + 1 == var8)
                {
                    this.drawTexturedModalRect(var25, var39, var24 + 45, 27, 9, 9);
                }
            }
        }
        else if (var37 instanceof EntityLivingBase)
        {
            this.mc.mcProfiler.endStartSection("mountHealth");
            EntityLivingBase var38 = (EntityLivingBase)var37;
            var39 = (int)Math.ceil((double)var38.func_110143_aJ());
            float var30 = var38.func_110138_aP();
            var26 = (int)(var30 + 0.5F) / 2;

            if (var26 > 30)
            {
                var26 = 30;
            }

            var25 = var13;

            for (int var31 = 0; var26 > 0; var31 += 20)
            {
                int var32 = Math.min(var26, 10);
                var26 -= var32;

                for (int var33 = 0; var33 < var32; ++var33)
                {
                    byte var34 = 52;
                    byte var35 = 0;

                    if (var6)
                    {
                        var35 = 1;
                    }

                    int var36 = var12 - var33 * 8 - 9;
                    this.drawTexturedModalRect(var36, var25, var34 + var35 * 9, 9, 9, 9);

                    if (var33 * 2 + 1 + var31 < var39)
                    {
                        this.drawTexturedModalRect(var36, var25, var34 + 36, 9, 9, 9);
                    }

                    if (var33 * 2 + 1 + var31 == var39)
                    {
                        this.drawTexturedModalRect(var36, var25, var34 + 45, 9, 9, 9);
                    }
                }

                var25 -= 10;
            }
        }

        this.mc.mcProfiler.endStartSection("air");

        if (this.mc.thePlayer.isInsideOfMaterial(Material.water))
        {
            var22 = this.mc.thePlayer.getAir();
            var39 = MathHelper.ceiling_double_int((double)(var22 - 2) * 10.0D / 300.0D);
            var24 = MathHelper.ceiling_double_int((double)var22 * 10.0D / 300.0D) - var39;

            for (var26 = 0; var26 < var39 + var24; ++var26)
            {
                if (var26 < var39)
                {
                    this.drawTexturedModalRect(var12 - var26 * 8 - 9, var18, 16, 18, 9, 9);
                }
                else
                {
                    this.drawTexturedModalRect(var12 - var26 * 8 - 9, var18, 25, 18, 9, 9);
                }
            }
        }

        this.mc.mcProfiler.endSection();
    }

    /**
     * Renders dragon's (boss) health on the HUD
     */
    private void renderBossHealth()
    {
        if (BossStatus.bossName != null && BossStatus.statusBarLength > 0)
        {
            --BossStatus.statusBarLength;
            FontRenderer var1 = this.mc.fontRenderer;
            ScaledResolution var2 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
            int var3 = var2.getScaledWidth();
            short var4 = 182;
            int var5 = var3 / 2 - var4 / 2;
            int var6 = (int)(BossStatus.healthScale * (float)(var4 + 1));
            byte var7 = 12;
            this.drawTexturedModalRect(var5, var7, 0, 74, var4, 5);
            this.drawTexturedModalRect(var5, var7, 0, 74, var4, 5);

            if (var6 > 0)
            {
                this.drawTexturedModalRect(var5, var7, 0, 79, var6, 5);
            }

            String var8 = BossStatus.bossName;
            var1.drawStringWithShadow(var8, var3 / 2 - var1.getStringWidth(var8) / 2, var7 - 10, 16777215);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.mc.func_110434_K().func_110577_a(field_110324_m);
        }
    }

    private void renderPumpkinBlur(int par1, int par2)
    {
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(false);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        this.mc.func_110434_K().func_110577_a(field_110328_d);
        Tessellator var3 = Tessellator.instance;
        var3.startDrawingQuads();
        var3.addVertexWithUV(0.0D, (double)par2, -90.0D, 0.0D, 1.0D);
        var3.addVertexWithUV((double)par1, (double)par2, -90.0D, 1.0D, 1.0D);
        var3.addVertexWithUV((double)par1, 0.0D, -90.0D, 1.0D, 0.0D);
        var3.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
        var3.draw();
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    }

    /**
     * Renders the vignette. Args: vignetteBrightness, width, height
     */
    private void renderVignette(float par1, int par2, int par3)
    {
        par1 = 1.0F - par1;

        if (par1 < 0.0F)
        {
            par1 = 0.0F;
        }

        if (par1 > 1.0F)
        {
            par1 = 1.0F;
        }

        this.prevVignetteBrightness = (float)((double)this.prevVignetteBrightness + (double)(par1 - this.prevVignetteBrightness) * 0.01D);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(false);
        GL11.glBlendFunc(GL11.GL_ZERO, GL11.GL_ONE_MINUS_SRC_COLOR);
        GL11.glColor4f(this.prevVignetteBrightness, this.prevVignetteBrightness, this.prevVignetteBrightness, 1.0F);
        this.mc.func_110434_K().func_110577_a(field_110329_b);
        Tessellator var4 = Tessellator.instance;
        var4.startDrawingQuads();
        var4.addVertexWithUV(0.0D, (double)par3, -90.0D, 0.0D, 1.0D);
        var4.addVertexWithUV((double)par2, (double)par3, -90.0D, 1.0D, 1.0D);
        var4.addVertexWithUV((double)par2, 0.0D, -90.0D, 1.0D, 0.0D);
        var4.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
        var4.draw();
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }

    private void func_130015_b(float par1, int par2, int par3)
    {
        if (par1 < 1.0F)
        {
            par1 *= par1;
            par1 *= par1;
            par1 = par1 * 0.8F + 0.2F;
        }

        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(false);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, par1);
        Icon var4 = Block.portal.getBlockTextureFromSide(1);
        this.mc.func_110434_K().func_110577_a(TextureMap.field_110575_b);
        float var5 = var4.getMinU();
        float var6 = var4.getMinV();
        float var7 = var4.getMaxU();
        float var8 = var4.getMaxV();
        Tessellator var9 = Tessellator.instance;
        var9.startDrawingQuads();
        var9.addVertexWithUV(0.0D, (double)par3, -90.0D, (double)var5, (double)var8);
        var9.addVertexWithUV((double)par2, (double)par3, -90.0D, (double)var7, (double)var8);
        var9.addVertexWithUV((double)par2, 0.0D, -90.0D, (double)var7, (double)var6);
        var9.addVertexWithUV(0.0D, 0.0D, -90.0D, (double)var5, (double)var6);
        var9.draw();
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    }

    /**
     * Renders the specified item of the inventory slot at the specified location. Args: slot, x, y, partialTick
     */
    private void renderInventorySlot(int par1, int par2, int par3, float par4)
    {
        ItemStack var5 = this.mc.thePlayer.inventory.mainInventory[par1];

        if (var5 != null)
        {
            float var6 = (float)var5.animationsToGo - par4;

            if (var6 > 0.0F)
            {
                GL11.glPushMatrix();
                float var7 = 1.0F + var6 / 5.0F;
                GL11.glTranslatef((float)(par2 + 8), (float)(par3 + 12), 0.0F);
                GL11.glScalef(1.0F / var7, (var7 + 1.0F) / 2.0F, 1.0F);
                GL11.glTranslatef((float)(-(par2 + 8)), (float)(-(par3 + 12)), 0.0F);
            }

            itemRenderer.renderItemAndEffectIntoGUI(this.mc.fontRenderer, this.mc.func_110434_K(), var5, par2, par3);

            if (var6 > 0.0F)
            {
                GL11.glPopMatrix();
            }

            itemRenderer.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.mc.func_110434_K(), var5, par2, par3);
        }
    }

    /**
     * The update tick for the ingame UI
     */
    public void updateTick()
    {
        if (this.recordPlayingUpFor > 0)
        {
            --this.recordPlayingUpFor;
        }

        ++this.updateCounter;

        if (this.mc.thePlayer != null)
        {
            ItemStack var1 = this.mc.thePlayer.inventory.getCurrentItem();

            if (var1 == null)
            {
                this.remainingHighlightTicks = 0;
            }
            else if (this.highlightingItemStack != null && var1.itemID == this.highlightingItemStack.itemID && ItemStack.areItemStackTagsEqual(var1, this.highlightingItemStack) && (var1.isItemStackDamageable() || var1.getItemDamage() == this.highlightingItemStack.getItemDamage()))
            {
                if (this.remainingHighlightTicks > 0)
                {
                    --this.remainingHighlightTicks;
                }
            }
            else
            {
                this.remainingHighlightTicks = 40;
            }

            this.highlightingItemStack = var1;
        }
    }

    public void setRecordPlayingMessage(String par1Str)
    {
        this.func_110326_a("Now playing: " + par1Str, true);
    }

    public void func_110326_a(String par1Str, boolean par2)
    {
        this.recordPlaying = par1Str;
        this.recordPlayingUpFor = 60;
        this.recordIsPlaying = par2;
    }

    /**
     * returns a pointer to the persistant Chat GUI, containing all previous chat messages and such
     */
    public GuiNewChat getChatGUI()
    {
        return this.persistantChatGUI;
    }

    public int getUpdateCounter()
    {
        return this.updateCounter;
    }
}

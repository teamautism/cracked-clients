package net.minecraft.src;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import org.lwjgl.opengl.GL11;

public class CustomFont
{
    private int texID;
    private int[] xPos;
    private int[] yPos;
    private int startChar;
    private int endChar;
    private FontMetrics metrics;

    public CustomFont(Minecraft mc, Object font, int size)
    {
        this(mc, font, size, 32, 126);
    }

    public CustomFont(Minecraft mc, Object font, int size, int startChar, int endChar)
    {
        this.startChar = startChar;
        this.endChar = endChar;
        this.xPos = new int[endChar - startChar];
        this.yPos = new int[endChar - startChar];
        BufferedImage img = new BufferedImage(256, 256, 2);
        Graphics g = img.getGraphics();

        try
        {
            if (font instanceof String)
            {
                String x = (String)font;

                if (x.contains("/"))
                {
                    g.setFont(Font.createFont(0, new File(x)).deriveFont((float)size));
                }
                else
                {
                    g.setFont(new Font(x, 0, size));
                }
            }
            else if (font instanceof InputStream)
            {
                g.setFont(Font.createFont(0, (InputStream)font).deriveFont((float)size));
            }
            else if (font instanceof File)
            {
                g.setFont(Font.createFont(0, (File)font).deriveFont((float)size));
            }
        }
        catch (Exception var11)
        {
            var11.printStackTrace();
        }

        g.setColor(new Color(255, 255, 255, 0));
        g.fillRect(0, 0, 256, 256);
        g.setColor(Color.white);
        this.metrics = g.getFontMetrics();
        int var12 = 2;
        int y = 2;

        for (int i = startChar; i < endChar; ++i)
        {
            g.drawString("" + (char)i, var12, y + g.getFontMetrics().getAscent());
            this.xPos[i - startChar] = var12;
            this.yPos[i - startChar] = y - this.metrics.getMaxDescent();
            var12 += this.metrics.stringWidth("" + (char)i) + 2;

            if (var12 >= 250 - this.metrics.getMaxAdvance())
            {
                var12 = 2;
                y += this.metrics.getMaxAscent() + this.metrics.getMaxDescent() + size / 2;
            }
        }
    }

    public void drawStringS(Gui gui, String text, int x, int y, int color)
    {
        int l = color & -16777216;
        int shade = (color & 16579836) >> 2;
        shade += l;
        this.drawString(gui, text, x + 1, y + 1, shade);
        this.drawString(gui, text, x, y, color);
    }

    public void drawString(Gui gui, String text, int x, int y, int color)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.texID);
        float red = (float)(color >> 16 & 255) / 255.0F;
        float green = (float)(color >> 8 & 255) / 255.0F;
        float blue = (float)(color & 255) / 255.0F;
        float alpha = (float)(color >> 24 & 255) / 255.0F;
        GL11.glColor4f(red, green, blue, alpha);
        int startX = x;

        for (int i = 0; i < text.length(); ++i)
        {
            char c = text.charAt(i);

            if (c == 92)
            {
                char type = text.charAt(i + 1);

                if (type == 110)
                {
                    y += this.metrics.getAscent() + 2;
                    x = startX;
                }

                ++i;
            }
            else
            {
                this.drawChar(gui, c, x, y);
                x = (int)((double)x + this.metrics.getStringBounds("" + c, (Graphics)null).getWidth());
            }
        }
    }

    public FontMetrics getMetrics()
    {
        return this.metrics;
    }

    public int getStringWidth(String text)
    {
        return (int)this.getBounds(text).getWidth();
    }

    public int getStringHeight(String text)
    {
        return (int)this.getBounds(text).getHeight();
    }

    private Rectangle getBounds(String text)
    {
        int w = 0;
        int h = 0;
        int tw = 0;

        for (int i = 0; i < text.length(); ++i)
        {
            char c = text.charAt(i);

            if (c == 92)
            {
                char type = text.charAt(i + 1);

                if (type == 110)
                {
                    h += this.metrics.getAscent() + 2;

                    if (tw > w)
                    {
                        w = tw;
                    }

                    tw = 0;
                }

                ++i;
            }
            else
            {
                tw += this.metrics.stringWidth("" + c);
            }
        }

        if (tw > w)
        {
            w = tw;
        }

        h += this.metrics.getAscent();
        return new Rectangle(0, 0, w, h);
    }

    private void drawChar(Gui gui, char c, int x, int y)
    {
        Rectangle2D bounds = this.metrics.getStringBounds("" + c, (Graphics)null);
        gui.drawTexturedModalRect(x, y, this.xPos[(byte)c - this.startChar], this.yPos[(byte)c - this.startChar], (int)bounds.getWidth(), (int)bounds.getHeight() + this.metrics.getMaxDescent());
    }
}
